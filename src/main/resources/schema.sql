DROP TABLE IF EXISTS note;
DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    id BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(50) UNIQUE,
    pass VARCHAR(255) NOT NULL,
    birthday DATE,
    country CHAR(3),
    city VARCHAR(100),
    street VARCHAR(255),
    gender ENUM('MALE','FEMALE','OTHER'),
    state ENUM('CURRENT','INACTIVE','DELETED') DEFAULT 'CURRENT',
    created TIMESTAMP default current_timestamp,
    updated TIMESTAMP,
    index users_email_idx(email),
    index users_first_last_name_idx(first_name, last_name)
)  ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO users (id, first_name, last_name, email, pass, birthday, country, city, street, gender, state) values
(1631374884591111001,'Davit','Abovyan','davit.abovyan@gmail.com','$2a$10$KUsGHCaM2YV1AZlnl9yUbONvioNoYh1i4JFnHm7FMXrAACvOV2Sci','2020-01-01T09:09:09','ARM','Yerevan','Some street','MALE','CURRENT'),
(1631374884591112001,'Poghos','Poghos','poghos.poghos@gmail.com','$2a$10$KUsGHCaM2YV1AZlnl9yUbONvioNoYh1i4JFnHm7FMXrAACvOV2Sci','2020-01-01T09:09:09','ARM','Yerevan','Some street','MALE','CURRENT'),
(1631374884591113001,'Jone','Davidson','jone.davidson@gmail.com','$2a$10$KUsGHCaM2YV1AZlnl9yUbONvioNoYh1i4JFnHm7FMXrAACvOV2Sci','2020-01-01T09:09:09','USA','Berlin','Some street','MALE','INACTIVE');

CREATE TABLE note
(
    id BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    title VARCHAR(50) not null,
    note VARCHAR(1000) NOT NULL,
    user_id BIGINT UNSIGNED NOT NULL,
    state ENUM('CURRENT','INACTIVE','DELETED') DEFAULT 'CURRENT',
    created TIMESTAMP default current_timestamp,
    updated TIMESTAMP,
    CONSTRAINT note_person_fk FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    index note_user_idx(user_id)
)  ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;