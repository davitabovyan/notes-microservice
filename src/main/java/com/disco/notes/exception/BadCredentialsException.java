package com.disco.notes.exception;

public class BadCredentialsException extends NoteApplicationException {
	public BadCredentialsException() {
		super("Provided email/pass do not match");
	}
}
