package com.disco.notes.exception;

public class BadRequestException extends NoteApplicationException {
	public BadRequestException(String message) {
		super(message);
	}
}
