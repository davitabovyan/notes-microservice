package com.disco.notes.exception;

import com.disco.notes.constant.EntityObject;

public class NotFoundException extends NoteApplicationException {
	public NotFoundException(EntityObject object, long id) {
		super(String.format("There is no %s with id: %d",object.name(),id));
	}
}
