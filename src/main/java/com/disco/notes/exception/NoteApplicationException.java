package com.disco.notes.exception;

public class NoteApplicationException extends RuntimeException {
	public NoteApplicationException(String message) {
		super(message);
	}
	public NoteApplicationException(String... keys) {
		super(String.join(",",keys));
	}
	public NoteApplicationException(String message, Throwable cause) {
		super(message, cause);
	}
}