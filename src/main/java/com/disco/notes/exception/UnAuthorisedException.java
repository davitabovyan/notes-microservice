package com.disco.notes.exception;

public class UnAuthorisedException extends NoteApplicationException {
	public UnAuthorisedException(long id) {
		super(String.format("The action is not authorised for object with id: %d", id));
	}
}
