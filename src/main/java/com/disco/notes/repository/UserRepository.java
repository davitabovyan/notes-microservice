package com.disco.notes.repository;


import com.disco.notes.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<UserEntity, Long>{
	@Query("SELECT u FROM UserEntity u where u.email = :email AND u.pass=:pass")
	Optional<UserEntity> findUser(@Param("email") String email, @Param("pass") String pass);

	@Query("SELECT u FROM UserEntity u where u.email = :email")
	Optional<UserEntity> findByUsername(@Param("email") String email);
}
