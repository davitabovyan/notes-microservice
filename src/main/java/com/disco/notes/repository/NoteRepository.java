package com.disco.notes.repository;

import com.disco.notes.entity.NoteEntity;
import com.disco.notes.entity.UserEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends BaseRepository<NoteEntity, Long> {
	List<NoteEntity> findAccountByUserOrderByIdDesc(@Param("user") UserEntity user);
}
