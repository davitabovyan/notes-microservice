package com.disco.notes.common;

import com.disco.notes.exception.NoteApplicationException;
import com.disco.notes.model.UserModel;
import org.springframework.security.core.context.SecurityContextHolder;

public class Util {

	private Util(){
		throw new NoteApplicationException("Utility class can't be instantiated");
	}

	public static UserModel getUserFromContext(){
		return (UserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
