package com.disco.notes.common;

import com.disco.notes.entity.NoteEntity;
import com.disco.notes.entity.UserEntity;
import com.disco.notes.model.NoteModel;
import com.disco.notes.model.UserModel;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;

public class Mapper {
	public static ModelMapper MAPPER = new ModelMapper();
	static {
		MAPPER.getConfiguration().setPropertyCondition(Conditions.isNotNull());
	}

	public static UserEntity map(UserModel model) {
		return MAPPER.map(model, UserEntity.class);
	}

	public static NoteEntity map(NoteModel model) {
		return MAPPER.map(model, NoteEntity.class);
	}


	public static UserModel map(UserEntity entity) {
		return MAPPER.map(entity, UserModel.class);
	}

	public static NoteModel map(NoteEntity entity) {
		return MAPPER.map(entity, NoteModel.class);
	}
}
