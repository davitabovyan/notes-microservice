package com.disco.notes.service.impl;

import com.disco.notes.constant.EntityObject;
import com.disco.notes.entity.NoteEntity;
import com.disco.notes.entity.UserEntity;
import com.disco.notes.exception.NotFoundException;
import com.disco.notes.repository.NoteRepository;
import com.disco.notes.service.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class NoteServiceImpl implements NoteService {

	private NoteRepository noteRepository;

	@Override
	public NoteEntity findById(long id) {
		return noteRepository.findById(id)
				.orElseThrow(()->new NotFoundException(EntityObject.NOTE, id));
	}

	@Override
	public void update(NoteEntity object) {
		noteRepository.save(object);
	}

	@Override
	public NoteEntity save(NoteEntity object) {
		return noteRepository.save(object);
	}

	@Override
	public List<NoteEntity> saveAll(List<NoteEntity> objects) {
		return null;
	}

	@Override
	public void deleteById(long id) {
		noteRepository.deleteById(id);
	}

	@Override
	public List<NoteEntity> findAllForUser(UserEntity user) {
		return noteRepository.findAccountByUserOrderByIdDesc(user);
	}
}
