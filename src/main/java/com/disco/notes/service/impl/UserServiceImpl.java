package com.disco.notes.service.impl;

import com.disco.notes.config.security.UserLogin;
import com.disco.notes.constant.EntityObject;
import com.disco.notes.dto.LoginDto;
import com.disco.notes.entity.UserEntity;
import com.disco.notes.exception.BadCredentialsException;
import com.disco.notes.exception.NotFoundException;
import com.disco.notes.model.UserModel;
import com.disco.notes.repository.UserRepository;
import com.disco.notes.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.disco.notes.common.Mapper.MAPPER;

@Data
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	@Override
	public UserModel login(LoginDto dto) {
		UserEntity user = userRepository.findUser(dto.getEmail(), dto.getPass())
				.orElseThrow(BadCredentialsException::new);
		return MAPPER.map(user, UserModel.class);
	}

	@Override
	public UserEntity findById(long id) {
		return userRepository.findById(id)
				.orElseThrow(()->new NotFoundException(EntityObject.USER,id));
	}

	@Override
	public void update(UserEntity object) {

	}

	@Override
	public UserEntity save(UserEntity object) {
		return userRepository.save(object);
	}

	@Override
	public List<UserEntity> saveAll(List<UserEntity> objects) {
		return null;
	}

	@Override
	public void deleteById(long id) {

	}

	@Override
	public UserLogin findUserByEmail(String email) {
		UserEntity user = userRepository.findByUsername(email).orElseThrow(BadCredentialsException::new);
		return new UserLogin().setUsername(user.getEmail()).setPassword(user.getPass()).setId(user.getId());

	}
}
