package com.disco.notes.service;

import com.disco.notes.entity.NoteEntity;
import com.disco.notes.entity.UserEntity;

import java.util.List;

public interface NoteService extends BaseService<NoteEntity> {
	List<NoteEntity> findAllForUser(UserEntity user);
}
