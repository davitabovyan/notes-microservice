package com.disco.notes.service;

import java.util.List;

public interface BaseService<T> {
	T findById(long id);
	void update(T object);
	T save(T object);
	List<T> saveAll(List<T> objects);
	void deleteById(long id);
}
