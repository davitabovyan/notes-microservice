package com.disco.notes.service;

import com.disco.notes.config.security.UserLogin;
import com.disco.notes.dto.LoginDto;
import com.disco.notes.entity.UserEntity;
import com.disco.notes.model.UserModel;

public interface UserService extends BaseService<UserEntity> {
	UserModel login(LoginDto dto);
	UserLogin findUserByEmail(String email);
}
