package com.disco.notes.entity;


import com.disco.notes.constant.EntityObject;
import com.disco.notes.constant.RecordState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter @Setter
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	@Transient
	private final EntityObject OBJ;

	@Id
	@GeneratedValue(generator = "custom")
	@GenericGenerator(name="custom", strategy = "com.disco.notes.config.Identifier")
	private long id;

	@Enumerated(EnumType.STRING)
	private RecordState state;

	private LocalDateTime created;
	private LocalDateTime updated;

	public BaseEntity(EntityObject entityObject) {
		this.state = RecordState.CURRENT;
		this.OBJ = entityObject;
	}

	public int getObjCodeIndex() {
		return this.OBJ.getIndex();
	}
}
