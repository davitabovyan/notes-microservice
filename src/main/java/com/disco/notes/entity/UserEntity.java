package com.disco.notes.entity;

import com.disco.notes.constant.EntityObject;
import com.disco.notes.constant.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Accessors(chain = true) @Getter @Setter
public class UserEntity extends BaseEntity {

	private String firstName;
	private String lastName;
	private String email;
	private String pass;
	private LocalDate birthday;

	@Embedded
	private Address address;

	@Enumerated(EnumType.STRING)
	private Gender gender;

	public UserEntity() {
		super(EntityObject.USER);
	}
}
