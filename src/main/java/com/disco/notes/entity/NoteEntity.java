package com.disco.notes.entity;

import com.disco.notes.constant.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "note")
@Accessors(chain = true) @Getter @Setter
public class NoteEntity extends BaseEntity {

	private String title;
	private String note;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public NoteEntity() {
		super(EntityObject.NOTE);
	}
}
