package com.disco.notes.controller;

import com.disco.notes.config.LocalDateAdapter;
import com.disco.notes.config.LocalDateTimeAdapter;
import com.disco.notes.exception.BadRequestException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.data.repository.NoRepositoryBean;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoRepositoryBean
public abstract class BaseController {

    static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter().nullSafe())
			.registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe())
			.create();

    void validateNotEmpty(String... args) {
        for(String arg : args){
            if(arg == null || arg.equals("")){
                throw new BadRequestException("Missing required fields");
            }
        }
    }

    void validateNotNull(Object... args) {
        for(Object arg : args){
            if(arg == null){
                throw new BadRequestException("Required field is null");
            }
        }
    }

}
