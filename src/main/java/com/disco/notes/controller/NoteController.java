package com.disco.notes.controller;

import com.disco.notes.common.Mapper;
import com.disco.notes.common.Util;
import com.disco.notes.entity.NoteEntity;
import com.disco.notes.entity.UserEntity;
import com.disco.notes.exception.UnAuthorisedException;
import com.disco.notes.model.NoteModel;
import com.disco.notes.service.NoteService;
import com.disco.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/v1/note", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class NoteController extends BaseController {

	private final NoteService noteService;
	private final UserService userService;

	@PostMapping
	public ResponseEntity<Map<String,Long>> create(@RequestBody @Valid NoteModel model) {

		validateNotEmpty(model.getNote());
		UserEntity user = userService.findById(Util.getUserFromContext().getId());

		model.setCreated(LocalDateTime.now());
		NoteEntity entity = noteService.save(Mapper.map(model).setUser(user));
		return new ResponseEntity<>(Collections.singletonMap("id",entity.getId()), HttpStatus.CREATED);
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<String> getAllNotesByUser(@PathVariable Long userId) {
		UserEntity user = userService.findById(userId);
		List<NoteModel> notes = noteService.findAllForUser(user)
				.stream().map(n->n.setUser(null)).map(Mapper::map).collect(Collectors.toList());
		return ResponseEntity.ok(GSON.toJson(notes));
	}

	@GetMapping("/{noteId}")
	public ResponseEntity<String> getNoteById(@PathVariable Long noteId) {
		NoteEntity note = noteService.findById(noteId);
		checkUserActionAuthoority(note);
		note.getUser().setPass(null);
		return ResponseEntity.ok(GSON.toJson(Mapper.map(note)));
	}

	@PutMapping("/{noteId}")
	public void updateNoteById(@RequestBody NoteModel model, @PathVariable long noteId) {
		NoteEntity oldNote = noteService.findById(noteId);
		checkUserActionAuthoority(oldNote);
		NoteEntity updatedNote = Mapper.map(model);
		if (model.getTitle() == null){
			updatedNote.setTitle(oldNote.getTitle());
		}
		if (model.getNote() == null) {
			updatedNote.setNote(oldNote.getNote());
		}
		updatedNote.setId(noteId);
		updatedNote.setUpdated(LocalDateTime.now());
		updatedNote.setCreated(oldNote.getCreated());
		updatedNote.setUser(oldNote.getUser()); // user should not be changed
		noteService.update(updatedNote);
	}

	@DeleteMapping("/{noteId}")
	public void deleteNoteById(@PathVariable Long noteId) {
		NoteEntity note = noteService.findById(noteId);
		checkUserActionAuthoority(note);
		noteService.deleteById(noteId);
	}

	private void checkUserActionAuthoority(NoteEntity note) {
		long userId = Util.getUserFromContext().getId();
		if (note.getUser().getId() != userId) {
			throw new UnAuthorisedException(note.getId());
		}
	}
}
