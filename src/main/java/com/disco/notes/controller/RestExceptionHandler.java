package com.disco.notes.controller;

import com.disco.notes.exception.BadCredentialsException;
import com.disco.notes.exception.BadRequestException;
import com.disco.notes.exception.NotFoundException;
import com.disco.notes.exception.UnAuthorisedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestExceptionHandler {
	@ExceptionHandler(UnAuthorisedException.class)
	protected ResponseEntity<Object> unauthorised(UnAuthorisedException ex, WebRequest request) {
		return new ResponseEntity<>("User is not authorised for the action", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(BadRequestException.class)
	protected ResponseEntity<Object> badRequest(BadRequestException ex, WebRequest request) {
		return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BadCredentialsException.class)
	protected ResponseEntity<Object> badCredentials(BadCredentialsException ex, WebRequest request) {
		return new ResponseEntity<>("Email/password are not match", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(NotFoundException.class)
	protected ResponseEntity<Object> noSuchRecord(NotFoundException ex, WebRequest request) {
		return new ResponseEntity<>("There is no such record with id: " + ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
	}
}
