package com.disco.notes.controller;

import com.disco.notes.dto.LoginDto;
import com.disco.notes.model.UserModel;
import com.disco.notes.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController extends BaseController{

	private final UserService userService;

	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody LoginDto dto){
		validateNotEmpty(dto.getPass(), dto.getEmail());
		UserModel user = userService.login(dto);
		return ResponseEntity.ok(GSON.toJson(user));
	}
}
