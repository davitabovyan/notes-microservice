package com.disco.notes.config;

import com.disco.notes.exception.NoteApplicationException;
import com.disco.notes.entity.BaseEntity;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

public class Identifier implements IdentifierGenerator {

    private static volatile int counter = 0;

    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o)
        throws HibernateException {

        if(o instanceof BaseEntity) {
            BaseEntity entity = (BaseEntity) o;
            return 1_000_000 * System.currentTimeMillis()
                    + 1000 * increment()
                    + entity.getObjCodeIndex();
        } else {
            throw new NoteApplicationException("Provided Entity object is not supported for id generation");
        }
    }

    private synchronized int increment(){
        return counter < 1000 ? ++counter : 0;
    }
}
