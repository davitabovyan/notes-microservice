package com.disco.notes.config;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class LocalDateTimeAdapter extends TypeAdapter<LocalDateTime> {
    DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-M-d'T'HH:mm:ss"))
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-MM-d'T'HH:mm:ss"))
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-M-dd'T'HH:mm:ss"))
            .toFormatter();

    @Override
    public void write(final JsonWriter jsonWriter, final LocalDateTime localDate ) throws IOException {
        jsonWriter.value(localDate.toString());
    }

    @Override
    public LocalDateTime read( final JsonReader jsonReader ) throws IOException {
        return LocalDateTime.parse(jsonReader.nextString(), formatter);
    }
}
