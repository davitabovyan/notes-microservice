package com.disco.notes.config.security;

import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TokenBasedAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    TokenBasedAuthenticationFilter(AuthenticationManager authenticationManager) {
        setAuthenticationManager(authenticationManager);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
											Authentication authentication) throws IOException {
        UserLogin user = (UserLogin) authentication.getPrincipal();
        long expire = System.currentTimeMillis() + SecurityConfig.TOKEN_LIFETIME;
        String token = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .claim(SecurityConfig.USER_ID, user.getId())
                .setSubject(user.getUsername())
                .setExpiration(new Date(expire))
                .signWith(Keys.hmacShaKeyFor(SecurityConfig.TOKEN_SECRET.getBytes()), SignatureAlgorithm.HS256)
                .compact();
        response.setContentType("application/json");
        Map<String, Object> object = new ConcurrentHashMap<>();
        object.put("token", token);
        object.put("expires", expire);
        response.getWriter().write(new Gson().toJson(object));
        response.getWriter().flush();

    }
}

