package com.disco.notes.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UsersConfiguration {

    @Bean
    public UserDetailsServiceImpl usersDAO() {
        return new UserDetailsServiceImpl();
    }
}

