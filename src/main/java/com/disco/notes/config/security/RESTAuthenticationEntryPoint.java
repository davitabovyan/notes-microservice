package com.disco.notes.config.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RESTAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        if(authException instanceof BadCredentialsException) {
            int status = Integer.parseInt(authException.getMessage());
            String message;
            if (status == HttpStatus.EXPECTATION_FAILED.value()) {
                message = "JWT token expired";
            } else {
                message = "JWT token invalid";
            }
            response.setStatus(status);
            response.getWriter().write(message);
            response.getWriter().flush();
            response.getWriter().close();
        } else response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }
}
