package com.disco.notes.config.security;

import com.disco.notes.model.UserModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class TokenBasedAuthorizationFilter extends BasicAuthenticationFilter {

    private static final Log LOG = LogFactory.getLog(TokenBasedAuthorizationFilter.class);
    private final AuthenticationEntryPoint authenticationEntryPoint;

    public TokenBasedAuthorizationFilter(AuthenticationManager authenticationManager, AuthenticationEntryPoint authenticationEntryPoint) {
        super(authenticationManager, authenticationEntryPoint);
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        boolean debug = LOG.isDebugEnabled();
        String authorizationToken = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (authorizationToken == null || authorizationToken.startsWith(SecurityConfig.TOKEN_PREFIX)) {
            SecurityContextHolder.clearContext();
        }
        if(null == authorizationToken){
            this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("401"));
            return;
        }
        authorizationToken = authorizationToken.replaceFirst(SecurityConfig.TOKEN_PREFIX, "");
        Jws<Claims> claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SecurityConfig.TOKEN_SECRET.getBytes())
                    .parseClaimsJws(authorizationToken);
        } catch (SignatureException ex){
            SecurityContextHolder.clearContext();
            if (debug) {
                LOG.debug("Authentication request for failed: " + authorizationToken);
            }
            this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("401"));
            return;
        } catch (ExpiredJwtException ex){
            SecurityContextHolder.clearContext();
            this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("417"));
            return;
        } catch (Exception ex){
            SecurityContextHolder.clearContext();
            this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("403"));
            return;
        }

        long id = (long) claims.getBody().get(SecurityConfig.USER_ID);
        UserModel user = new UserModel();
        user.setEmail(claims.getBody().getSubject());
        user.setId(id);

        SecurityContextHolder.getContext()
                .setAuthentication(
                        new UsernamePasswordAuthenticationToken(
                                user,
                                null,
                                Collections.singleton(()->"ROLE_USER"))
                );

        chain.doFilter(request, response);
    }
}
