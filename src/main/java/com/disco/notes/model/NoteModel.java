package com.disco.notes.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class NoteModel extends BaseModel {
	@NotNull
	@Size(max = 50, message = "Title max size is 50")
	private String title;
	@NotNull
	@Size(max = 1000, message = "Note max size is 1000")
	private String note;
	private UserModel user;
}
