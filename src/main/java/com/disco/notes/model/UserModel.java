package com.disco.notes.model;

import com.disco.notes.constant.Gender;
import com.disco.notes.entity.Address;
import com.disco.notes.entity.NoteEntity;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

@Data
public class UserModel extends BaseModel {
	private String firstName;
	private String lastName;
	@Email
	private String email;
	@NotNull
	@Size(min = 8, message = "Password length should be more then 8")
	private String pass;
	private LocalDate birthday;
	private Address address;
	private Gender gender;
	private Set<NoteEntity> notes;
}
