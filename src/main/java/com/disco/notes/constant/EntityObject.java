package com.disco.notes.constant;

import com.disco.notes.exception.NoteApplicationException;
import com.disco.notes.model.NoteModel;
import com.disco.notes.model.UserModel;

public enum EntityObject {

	USER(1, 0b10L, UserModel.class),
	NOTE(2, 0b100L, NoteModel.class),
	;

	private final long access;
	private final int index;
	private final Class<?> clazz;

	EntityObject(int index, long access, Class<?> clazz){
		this.index = index;
		this.access = access;
		this.clazz = clazz;
	}

	public String getCode(){
		return this.name().toLowerCase();
	}
	public long getAccess(){
		return access;
	}
	public int getIndex(){
		return index;
	}
	public Class<?> getClazz(){
		return clazz;
	}

	public static EntityObject getByIndex(int index){
		for(EntityObject obj : EntityObject.values()){
			if(obj.getIndex() == index) return obj;
		}
		throw new NoteApplicationException(String.format("Incorrect index %d for EntityObject", index));
	}

	public static EntityObject getByClass(Class<?> clazz){
		for(EntityObject obj : EntityObject.values()){
			if(clazz.equals(obj.getClazz())) return obj;
		}
		throw new NoteApplicationException(String.format("Incorrect class %d for EntityObject", clazz));
	}

	public static EntityObject getByCode(String objectCode){
		try {
			return EntityObject.valueOf(objectCode.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new NoteApplicationException("error.unsupported_object", objectCode);
		}
	}
}
