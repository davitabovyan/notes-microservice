package com.disco.notes.constant;

public enum Gender {
    MALE,
    FEMALE,
    OTHER,
    ;
}
