package com.disco.notes.constant;

public enum RecordState {
    CURRENT,
    INACTIVE,
    DELETED,
    ;
}
